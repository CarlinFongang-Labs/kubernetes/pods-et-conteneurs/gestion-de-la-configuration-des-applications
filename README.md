# Gestion de la configuration des applications


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs 

Nous allons parler de la gestion de la configuration des applications. Voici un bref aperçu de ce que nous allons aborder dans cette leçon. Nous allons parler de ce qu'est la configuration des applications, puis nous aborderons les fonctionnalités de Kubernetes qui nous permettent de gérer cette configuration.

Nous commencerons par parler des ConfigMaps, puis nous aborderons les secrets, qui nous permettent de stocker des données de configuration sensibles. Ensuite, nous parlerons de différentes méthodes pour transmettre ces données à nos conteneurs, notamment les variables d'environnement et les volumes de configuration. Enfin, nous ferons une démonstration pratique pour montrer à quoi ressemblent tous ces concepts dans un cluster Kubernetes réel.

#### Qu'est-ce que la Configuration des Applications ?
Lorsque vous exécutez une application dans Kubernetes, vous voudrez probablement, à un moment donné, passer des valeurs dynamiques à vos applications au moment de l'exécution pour contrôler leur comportement. Ce processus est connu sous le nom de configuration des applications. Il s'agit donc de transmettre des données à nos conteneurs pour contrôler ce que ces conteneurs font et comment ils fonctionnent.

Il existe deux principales façons de stocker les données de configuration dans Kubernetes, et la première est le ConfigMap. Les ConfigMaps nous permettent de stocker des données sous forme de paires clé-valeur, et nous pouvons ensuite transmettre ces données de configuration à nos applications conteneurisées.

#### ConfigMaps
Voici à quoi ressemble une définition de ConfigMap. La section la plus importante à observer est la section `data`, qui est essentiellement des données YAML. C'est une carte de paires clé-valeur. Nous avons des paires clé-valeur simples, et, bien sûr, comme pour toute donnée YAML, nous pouvons avoir des paires clé-valeur imbriquées et même définir des données multi-lignes. Ces données multi-lignes sont particulièrement utiles si vous souhaitez définir quelque chose comme un fichier de configuration que vous souhaitez ensuite rendre disponible pour l'un de vos conteneurs. Voilà à quoi ressemble un ConfigMap.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-configmap
data:
  key1: value1
  key2: value2
  key3:
    subkey:
      morekeys: data
      evenmore: some more data
  key4: |
    Test
    multiple lines
    more lines
```


#### Secrets
Il existe une autre façon de stocker les données de configuration dans Kubernetes, appelée secret. Les secrets sont très similaires aux ConfigMaps, mais ils sont conçus pour stocker de manière sécurisée des données sensibles. Donc, si vos données de configuration incluent des éléments tels que des mots de passe ou des clés API, vous voudrez probablement utiliser un secret au lieu d'un ConfigMap, juste pour garantir que ces données soient stockées de manière sécurisée.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secrettype: Opaque
data:
  username: <base64 String 1>
  password: <base64 String 2>
```

#### Transmission des Données de Configuration
Nous avons nos ConfigMaps et nos secrets, mais comment prendre ces données et les utiliser ? Comment transmettre ces données à nos conteneurs ? Il existe plusieurs façons de le faire, et la première que nous allons aborder est les variables d'environnement. Nous pouvons transmettre les données des ConfigMaps et des secrets à nos conteneurs en utilisant des variables d'environnement, et ces variables seront visibles à l'exécution pour le processus du conteneur.

Voici une idée générale de ce à quoi cela ressemble dans votre spécification de pod. Dans la spécification du conteneur, nous définissons une variable d'environnement que nous tirons d'une clé particulière dans un ConfigMap, et cette variable sera ensuite passée comme variable d'environnement au processus du conteneur.

#### Configuration des volumes
L'autre façon de transmettre des données à nos conteneurs est d'utiliser des volumes de configuration. Avec le volume de configuration, nos données de configuration provenant de notre ConfigMap ou secret sont passées sous forme de volume monté, ce qui signifie que nos données de configuration apparaîtront sous forme de fichiers disponibles dans le système de fichiers du conteneur. Concrètement, cela signifie que nous aurons un fichier pour chaque clé de niveau supérieur dans les données de configuration, et le contenu de ce fichier sera toutes les données et sous-clés associées à cette clé de niveau supérieur.

```yaml
...
volumes:
- name: secret-vol
  secret:
    secretName: my-secret
```

Si cela ne vous semble pas clair pour le moment, ne vous inquiétez pas. Nous le verrons plus clairement lors de la démonstration pratique. Passons directement à la démonstration pratique pour voir à quoi cela ressemble en pratique de configurer nos applications en utilisant les ConfigMaps et les secrets.

#### Démonstration Pratique

1. Création du fichier de ConfigMap

Une fois connecté ici à mon serveur de plan de contrôle Kubernetes, et je vais commencer par créer un ConfigMap. Je vais créer un fichier appelé `my-configmap.yml`, et je vais simplement coller le contenu de ce ConfigMap. 

```yaml
ssh -i id_rsa user@public_ip_address
```

```yaml
nano my-configmap.yml
```

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-configmap
data:
  key1: Hello, world!
  key2: |
    Test
    multiple lines
    more lines
```

2. Création du ConfigMap


C'est un ConfigMap simple avec deux clés. La première clé a la valeur "hello world". La deuxième clé a une valeur multi-ligne avec juste quelques données de test sur plusieurs lignes. Je vais enregistrer et quitter ce fichier, et bien sûr, je peux créer mon ConfigMap comme d'habitude avec `kubectl create -f`, et ainsi, j'ai créé mon ConfigMap.

```yaml
kubectl create -f my-configmap.yml
```
>![Alt text](img/image.png)
*Création du configmap réussi*

3. Describe du ConfigMap

Je vais faire un `kubectl describe` sur mon ConfigMap, 

```yaml
kubectl describe configmap my-configmap
```

>![Alt text](img/image-1.png)
*Contenu du configmap crée*


et comme vous pouvez le voir, nous pouvons effectivement voir le contenu de nos données de configuration en utilisant `kubectl describe`. Ensuite, je vais créer un secret, et il existe plusieurs façons de créer un secret, mais je vais le faire en utilisant un fichier YAML.


4. Création du manieste my-secret.yml

Je vais créer ce fichier `my-secret.yml`, et je vais y coller le contenu de ce fichier YAML. Maintenant, dans ma section `data`, j'ai quelques clés, mais je n'ai pas encore entré les données réelles pour ces clés, et la raison en est que lorsque nous créons un secret via un fichier YAML comme celui-ci, 

```yaml
nano my-secret.yml
```


```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
data:
  secretkey1: <base64 String 1>
  secretkey2: <base64 String 2>
```

nous devons encoder en **base64** nos valeurs de secret. Je vais donc enregistrer et quitter ce fichier, et encoder en base64 la première clé. Je vais faire `echo -n`, et la valeur de la première clé sera simplement "secret", et je vais passer cela à base64. 

```yaml
echo -n 'secret' | base64
```
Résultat : c2VjcmV0
et 

```yaml
echo -n 'secondsecret' | base64
```
Résultat : c2Vjb25kc2VjcmV0

>![Alt text](img/image-2.png)
*Conversion des secrets en base64*

Voilà ma valeur en base64. Je vais rouvrir `my-secret.yml` et coller cette valeur base64 pour la première clé, puis refaire le processus pour la deuxième clé. Cette fois, je vais mettre la valeur "secondsecret". Voilà ma chaîne base64. Je vais éditer `my-secret.yml` à nouveau et coller cette deuxième chaîne base64. 

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
data:
  secretkey1: c2VjcmV0
  secretkey2: c2Vjb25kc2VjcmV0
```

5. Création du secret

Je vais enregistrer et quitter ce fichier, et nous pouvons créer le secret en utilisant `kubectl create`, et ainsi, j'ai créé ce secret.

```yaml
kubectl create my-secret.yml
```

>![Alt text](img/image-3.png)
*Création du secret réussie*



6. Creation de pod et transmission des données de configuration comme variable d'environnement


Maintenant que nous avons créé un ConfigMap et un secret, voyons comment nous pouvons réellement transmettre ces données de configuration à nos pods. Je vais commencer par passer des données de configuration en utilisant des variables d'environnement. Je vais créer un fichier de définition de pod appelé `env-pod.yml` pour environment, et je vais commencer avec une définition de pod assez basique. 

```bash
nano env-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: env-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ['sh', '-c', 'echo "configmap: $CONFIGMAPVAR secret: $SECRETVAR"']
    env:
    - name: CONFIGMAPVAR
      valueFrom:
        configMapKeyRef:
          name: my-configmap
          key: key1
    - name: SECRETVAR
      valueFrom:
        secretKeyRef:
          name: my-secret
          key: secretkey1
```

Nous exécutons busybox, et pour ma commande, je vais simplement afficher quelques informations à la console. Je vais donc afficher "ConfigMap:" suivi de cette variable d'environnement `CONFIGMAPVAR`, puis une autre variable d'environnement `SECRETVAR`. Nous allons simplement afficher les valeurs de ces deux variables d'environnement pour montrer que nous transmettons effectivement nos données de configuration au conteneur et que le processus du conteneur à l'exécution peut voir ces variables d'environnement.

Comment allons-nous fournir ces variables d'environnement à partir de notre ConfigMap et de notre secret ? 
Nous pouvons le faire avec le bloc `env` ici, qui fait partie de nos spécifications de conteneur. 
Nous pouvons avoir un `env` séparé pour chaque conteneur, et c'est un tableau. Je peux donc avoir autant de variables d'environnement que je veux, et la première, je vais l'appeler `CONFIGMAPVAR`. C'est cette variable que nous allons imprimer ici dans notre commande. Le nom de la variable d'environnement ici dans notre définition détermine le nom de la variable d'environnement qui sera exposée au processus du conteneur. Ensuite, j'utiliserai cette clé `valueFrom` pour indiquer au cluster d'où la valeur de cette variable d'environnement va être tirée, et j'utiliserai `configMapKeyRef` pour indiquer que je vais référencer une clé spécifique d'un ConfigMap. Il va donc chercher une clé dans un ConfigMap et quelle que soit la valeur de cette clé, elle deviendra la valeur de ma variable d'environnement.

Je dois d'abord spécifier le nom du ConfigMap, qui est simplement celui que j'ai créé précédemment, `my-configmap`, puis le nom de la clé. Je vais spécifier une des clés que j'ai créées précédemment, appelée `key1`. La valeur de `key1` dans ce ConfigMap sera donc la valeur de cette variable d'environnement.

Je vais ajouter une autre variable d'environnement ici, pour montrer comment nous pouvons faire la même chose avec un secret, et comme vous pouvez le voir, c'est presque exactement la même chose. La seule différence est que j'ai `secretKeyRef` ici au lieu de `configMapKeyRef`. Je fais toujours référence au nom du secret et au nom de la clé dans le secret. Dans ce cas, il va chercher un secret appelé `my-secret` et une clé appelée `secretkey1`.

Je vais enregistrer et quitter ce fichier, et créer ce pod. 

```bash
kubectl create -f env-pod.yml
```
>![Alt text](img/image-4.png)
*Création du pod réussi*

Puis, regardons les logs de notre pod. Si nous regardons les logs, nous devrions pouvoir voir la sortie de notre commande. Nous allons faire `kubectl logs` sur `env-pod`, 

```bash
kubectl logs env-pod
```

>![Alt text](img/image-5.png)

et effectivement, pour `ConfigMap`, je vois "hello world" comme valeur, et puis ma première valeur de secret que j'ai incluse pour cette première clé dans `my-secret` est simplement le mot "secret". Nous pouvons donc voir que nos données de ConfigMap et nos données de secret sont effectivement exposées au processus du conteneur en tant que variables d'environnement.

Il y a une autre façon d'exposer nos données au conteneur, et c'est par l'utilisation de volumes de configuration.

7. Volume de configuration 

```bash
nano volume-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ['sh', '-c', 'while true; do sleep 3600; done']
    volumeMounts:
      - name: configmap-volume
        mountPath: /etc/config/configmap
      - name: secret-volume
        mountPath: /etc/config/secret
  volumes:
  - name: configmap-volume
    configMap:
      name: my-configmap
  - name: secret-volume
    secret:
      secretName: my-secret
```

```bash
kubectl create -f volume-pod.yml
```

>![Alt text](img/image-6.png)
*Volume pod crée*

Allons vérifier qeu nous pouvons réellement voir nos données de configuration à l'intérieur de notre pod 

commande usuelle

```bash
kubectl exec volume-pod -- ls /etc/config/configmap
kubectl exec volume-pod -- cat /etc/config/configmap/key1
kubectl exec volume-pod -- cat /etc/config/configmap/key2
kubectl exec volume-pod -- ls /etc/config/secret
kubectl exec volume-pod -- cat /etc/config/secret/secretkey1
kubectl exec volume-pod -- cat /etc/config/secret/secretkey2
```

>![Alt text](img/image-7.png)
*Résultat des différents check dans le pod*

# Reférences 

https://kubernetes.io/docs/concepts/configuration/configmap/

https://kubernetes.io/docs/concepts/configuration/secret/